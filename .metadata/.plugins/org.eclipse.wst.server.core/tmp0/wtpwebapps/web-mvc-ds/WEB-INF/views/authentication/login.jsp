<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page isELIgnored="false"%>
<div class="container-fluid">
	<form:form
		action="${pageContext.request.contextPath}/authentication/login"
		method="post" modelAttribute="userDto">
		Username:<form:input type="text" path="username" id="usernameId" />
		<br />
		<form:errors path="username" cssClass="error" />
		<p />
		Password::<form:input type="password" path="password" id="passwordId" />
		<br />
		<form:errors path="password" cssClass="error" />
		<p />
		<!-- <button id="Login">Login</button> -->
		<button class="btn btn-success w-100" type="submit">Login</button>
	</form:form>
</div>

