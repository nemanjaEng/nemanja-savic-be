package com.engineering.nemanja.savic.be.dto;

import java.io.Serializable;
import java.sql.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;

public class ProfessorDto implements Serializable{

	private static final long serialVersionUID = 9105068127036740345L;

	private Long profesorId;
		
	@Size(min = 3, max = 30, message = "First name minimum 3 characters")
	@NotEmpty(message = "First name is required...")
	private String firstName;
	
	@Size(min = 3, max = 30, message = "Last name minimum 3 characters")
	@NotEmpty(message = "Last name is required...")
	private String lastName;
	
	@Email(message = "Email should be valid")
	private String email;
	
	@Size(min = 3, max = 30, message = "Address minimum 3 characters")
	private String address;
	
	@Size(min = 6, max = 15, message = "Phone number minimum 6 characters")
	private String phone;
	@PastOrPresent(message = "Reelection Date must be before today!")
	private Date reelectionDate;
	private CityDto city;
	private TitleDto title;
	
	public ProfessorDto() {
		
	}

	public Long getProfesorId() {
		return profesorId;
	}

	public void setProfesorId(Long profesorId) {
		this.profesorId = profesorId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getReelectionDate() {
		return reelectionDate;
	}

	public void setReelectionDate(Date reelectionDate) {
		this.reelectionDate = reelectionDate;
	}

	public CityDto getCity() {
		return city;
	}

	public void setCity(CityDto city) {
		this.city = city;
	}

	public TitleDto getTitle() {
		return title;
	}

	public void setTitle(TitleDto title) {
		this.title = title;
	}

	

	public ProfessorDto(Long profesorId,
			@Size(min = 3, max = 30, message = "First name minimum 3 characters") @NotEmpty(message = "First name is required...") String firstName,
			@Size(min = 3, max = 30, message = "Last name minimum 3 characters") @NotEmpty(message = "Last name is required...") String lastName,
			@Email(message = "Email should be valid") String email,
			@Size(min = 3, max = 30, message = "Address minimum 3 characters") String address,
			@Size(min = 6, max = 15, message = "Phone number minimum 6 characters") String phone, Date reelectionDate,
			CityDto city, TitleDto title) {
		super();
		this.profesorId = profesorId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.phone = phone;
		this.reelectionDate = reelectionDate;
		this.city = city;
		this.title = title;
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((profesorId == null) ? 0 : profesorId.hashCode());
		result = prime * result + ((reelectionDate == null) ? 0 : reelectionDate.hashCode());

		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfessorDto other = (ProfessorDto) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (profesorId == null) {
			if (other.profesorId != null)
				return false;
		} else if (!profesorId.equals(other.profesorId))
			return false;
		if (reelectionDate == null) {
			if (other.reelectionDate != null)
				return false;
		} else if (!reelectionDate.equals(other.reelectionDate))
			return false;
;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProfessorDto [profesorId=" + profesorId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", email=" + email + ", address=" + address + ", phone=" + phone + ", reelectionDate="
				+ reelectionDate + ", city=" + city + ", title=" + title + "]";
	}

	
	
}
