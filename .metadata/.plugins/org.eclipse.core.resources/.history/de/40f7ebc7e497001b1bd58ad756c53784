package com.engineering.nemanja.savic.be.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "professor")
public class ProfessorEntity implements Serializable {

	private static final long serialVersionUID = 6343128357714764791L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="professor_id", nullable = false)
	private Long profesorId;
	
	@Column(name="first_name", nullable = false, unique = false,length = 30)
	private String firstName;
	
	@Column(name="last_name", nullable = false, unique = false,length = 30)
	private String lastName;
	
	@Column(name="email", nullable = true, unique = true,length = 30 )
	private String email;
	
	@Column(name="address", nullable = true, unique = false,length = 50)
	private String address;
	
	@Column(name="phone", nullable = true, unique = false,length = 15)
	private String phone;
	
	@Column(name="reelection_date", nullable = false, unique = false,length = 15)
	private Date reelectionDate;
	
	@ManyToOne
	@JoinColumn(name = "postal_code")
	private CityEntity city;
	
	@ManyToOne
	@JoinColumn(name = "title_number", nullable = false)
	private TitleEntity title;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "subject_id")
	private SubjectEntity subject;
	
	public ProfessorEntity() {
		
	}

	public ProfessorEntity(Long profesorId, String firstName, String lastName, String email, String address,
			String phone, Date reelectionDate, CityEntity city, TitleEntity title, SubjectEntity subject) {
		super();
		this.profesorId = profesorId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.phone = phone;
		this.reelectionDate = reelectionDate;
		this.city = city;
		this.title = title;
		this.subject = subject;
	}

	public Long getProfesorId() {
		return profesorId;
	}

	public void setProfesorId(Long profesorId) {
		this.profesorId = profesorId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getReelectionDate() {
		return reelectionDate;
	}

	public void setReelectionDate(Date reelectionDate) {
		this.reelectionDate = reelectionDate;
	}

	public CityEntity getCity() {
		return city;
	}

	public void setCity(CityEntity city) {
		this.city = city;
	}

	public TitleEntity getTitle() {
		return title;
	}

	public void setTitle(TitleEntity title) {
		this.title = title;
	}

	public SubjectEntity getSubject() {
		return subject;
	}

	public void setSubject(SubjectEntity subject) {
		this.subject = subject;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((profesorId == null) ? 0 : profesorId.hashCode());
		result = prime * result + ((reelectionDate == null) ? 0 : reelectionDate.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfessorEntity other = (ProfessorEntity) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (profesorId == null) {
			if (other.profesorId != null)
				return false;
		} else if (!profesorId.equals(other.profesorId))
			return false;
		if (reelectionDate == null) {
			if (other.reelectionDate != null)
				return false;
		} else if (!reelectionDate.equals(other.reelectionDate))
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProfessorEntity [profesorId=" + profesorId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", email=" + email + ", address=" + address + ", phone=" + phone + ", reelectionDate="
				+ reelectionDate + ", city=" + city + ", title=" + title + ", subject=" + subject + "]";
	}

	

	
}
