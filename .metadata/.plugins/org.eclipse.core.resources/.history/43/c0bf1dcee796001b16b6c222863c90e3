package com.engineering.nemanja.savic.be.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.engineering.nemanja.savic.be.dao.ProfessorDao;
import com.engineering.nemanja.savic.be.dao.SubjectDao;
import com.engineering.nemanja.savic.be.dto.ProfessorDto;
import com.engineering.nemanja.savic.be.dto.SubjectDto;
import com.engineering.nemanja.savic.be.entity.CityEntity;
import com.engineering.nemanja.savic.be.entity.StudentEntity;
import com.engineering.nemanja.savic.be.entity.SubjectEntity;
import com.engineering.nemanja.savic.be.mapper.SubjectMapper;
import com.engineering.nemanja.savic.be.service.SubjectService;
import com.engineering.nemanja.savic.exception.EntityNotPresent;
import com.engineering.nemanja.savic.exception.ExistEntityException;

@Service
@Transactional
public class SubjectServiceImpl implements SubjectService{

	private final SubjectDao subjectDao;
	private final ProfessorDao professorDao;
	
	private final SubjectMapper subjectMapper = Mappers.getMapper(SubjectMapper.class);
	
	@Autowired
	public SubjectServiceImpl(SubjectDao subjectDao, ProfessorDao professorDao) {
		super();
		this.subjectDao = subjectDao;
		this.professorDao = professorDao;
	}
	
	
	@Override
	public List<SubjectDto> findAll() {
		List<SubjectEntity> entites = subjectDao.findAll();
		return entites.stream().map(entity -> {
			return subjectMapper.toSubjectDto(entity);
		}).collect(Collectors.toList());
	}

	

	@Override
	public void deleteById(Long id) {
		
		subjectDao.deleteById(id);
	}

	@Override
	public SubjectDto save(SubjectDto subjectDto) throws ExistEntityException {
		
		SubjectEntity subjectEntity = subjectMapper.toSubject(subjectDto);
		
		subjectEntity = subjectDao.save(subjectEntity);
		return subjectMapper.toSubjectDto(subjectEntity);
		
	}

	@Override
	public Optional<SubjectDto> findById(Long id) {
		Optional<SubjectEntity> subjectEntity = subjectDao.findById(id);
		if (subjectEntity.isPresent()) {
			return Optional.of(subjectMapper.toSubjectDto(subjectEntity.get()));
		}
		return Optional.empty();
	}

	@Override
	public SubjectDto update(SubjectDto subjectDto) throws EntityNotPresent {
		
		Optional<SubjectEntity> subjectEntityOpt = subjectDao.findById(subjectDto.getId());
		if (subjectEntityOpt == null)
			throw new EntityNotPresent(subjectDto, "Subject doesnt exist!");

		SubjectEntity subjectEntity = subjectEntityOpt.get();
		subjectEntity.setId(subjectDto.getId());
		subjectEntity.setName(subjectDto.getName());
		subjectEntity.setDescription(subjectDto.getDescription());
		subjectEntity.setNoOfeSP(subjectDto.getNoOfeSP());
		subjectEntity.setSemester(subjectDto.getSemester());
		subjectEntity.setYearOfStudy(subjectDto.getYearOfStudy());
		subjectEntity.setProfessors(subjectDto.getProfessors());
		return subjectMapper.toSubjectDto(subjectEntity);
	}

	@Override
	public Page<SubjectDto> findByPage(Pageable pageable) {
		Page<SubjectDto> entites = subjectDao.findAll(pageable).map(subjectMapper::toSubjectDto);
		return entites;
	}

}
