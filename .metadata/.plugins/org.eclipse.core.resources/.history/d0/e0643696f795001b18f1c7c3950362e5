package com.engineering.nemanja.savic.be.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.engineering.nemanja.savic.be.dao.CityDao;
import com.engineering.nemanja.savic.be.dao.TitleDao;
import com.engineering.nemanja.savic.be.dto.TitleDto;
import com.engineering.nemanja.savic.be.entity.CityEntity;
import com.engineering.nemanja.savic.be.entity.TitleEntity;
import com.engineering.nemanja.savic.be.mapper.CityMapper;
import com.engineering.nemanja.savic.be.mapper.TitleMapper;
import com.engineering.nemanja.savic.be.service.TitleService;

@Service
@Transactional
public class TitleServiceImpl implements TitleService{
	
	
		private final TitleDao titleDao;
	
	private final TitleMapper titleMapper = Mappers.getMapper(TitleMapper.class);
	
	@Autowired
	public TitleServiceImpl(TitleDao titleDao) {
		this.titleDao = titleDao;
	}
	
	
	@Override
	public List<TitleDto> findAll() {
		List<TitleEntity> entites = titleDao.findAll();
		return entites.stream().map(entity -> {
			return titleMapper.toTitleDto(entity);
		}).collect(Collectors.toList());
	}

	@Override
	public Optional<TitleDto> findById(Long id) {
		Optional<TitleEntity> title = titleDao.findById(id);
		if (title.isPresent()) {
			return Optional.of(titleMapper.toTitleDto(title.get()));
		}
		return Optional.empty();
	}

}
