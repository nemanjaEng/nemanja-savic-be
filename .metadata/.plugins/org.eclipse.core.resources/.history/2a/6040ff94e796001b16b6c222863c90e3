package com.engineering.nemanja.savic.be.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import com.engineering.nemanja.savic.be.entity.ProfessorEntity;
import com.engineering.nemanja.savic.be.entity.Semester;


public class SubjectDto implements Serializable {

	private static final long serialVersionUID = -6782701089097557916L;

	private Long id;

	@Size(min = 3, max = 30, message = "Minimal number of characters is 3")
	private String name;
	private String description;
	@Min(value = 1)
	@Max(value = 9)
	private Integer noOfeSP;

	@Min(value = 1)
	@Max(value = 5)
	private Integer yearOfStudy;

	private Semester semester;

	private List<ProfessorDto> professors;

	public SubjectDto() {

	}

	public SubjectDto(Long id, @Size(min = 3, max = 30, message = "Minimal number of characters is 3") String name,
			String description, @Min(1) @Max(9) Integer noOfeSP, @Min(1) @Max(5) Integer yearOfStudy, Semester semester,
			List<ProfessorEntity> professors) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.noOfeSP = noOfeSP;
		this.yearOfStudy = yearOfStudy;
		this.semester = semester;
		this.professors = professors;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((noOfeSP == null) ? 0 : noOfeSP.hashCode());
		result = prime * result + ((professors == null) ? 0 : professors.hashCode());
		result = prime * result + ((semester == null) ? 0 : semester.hashCode());
		result = prime * result + ((yearOfStudy == null) ? 0 : yearOfStudy.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubjectDto other = (SubjectDto) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (noOfeSP == null) {
			if (other.noOfeSP != null)
				return false;
		} else if (!noOfeSP.equals(other.noOfeSP))
			return false;
		if (professors == null) {
			if (other.professors != null)
				return false;
		} else if (!professors.equals(other.professors))
			return false;
		if (semester != other.semester)
			return false;
		if (yearOfStudy == null) {
			if (other.yearOfStudy != null)
				return false;
		} else if (!yearOfStudy.equals(other.yearOfStudy))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getNoOfeSP() {
		return noOfeSP;
	}

	public void setNoOfeSP(Integer noOfeSP) {
		this.noOfeSP = noOfeSP;
	}

	public Integer getYearOfStudy() {
		return yearOfStudy;
	}

	public void setYearOfStudy(Integer yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}

	public Semester getSemester() {
		return semester;
	}

	public void setSemester(Semester semester) {
		this.semester = semester;
	}

	public List<ProfessorEntity> getProfessors() {
		return professors;
	}

	public void setProfessors(List<ProfessorEntity> professors) {
		this.professors = professors;
	}

	@Override
	public String toString() {
		return "SubjectDto [id=" + id + ", name=" + name + ", description=" + description + ", noOfeSP=" + noOfeSP
				+ ", yearOfStudy=" + yearOfStudy + ", semester=" + semester + ", professors=" + professors + "]";
	}

    public void addProfessor(ProfessorDto professorDto) {
        if (professors == null) {
        	professors = new ArrayList<>();
        }

        professors.add(professorDto);
    }

}
