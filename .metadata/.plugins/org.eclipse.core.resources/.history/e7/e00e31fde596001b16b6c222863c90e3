package com.engineering.nemanja.savic.be.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.engineering.nemanja.savic.be.dto.StudentDto;
import com.engineering.nemanja.savic.exception.EntityNotPresent;
import com.engineering.nemanja.savic.exception.ExistEntityException;

public interface StudentService {

	List<StudentDto> findAll();

	void deleteById(Long id);

	StudentDto save(StudentDto studentDto) throws ExistEntityException;

	Optional<StudentDto> findById(Long id);
	
	StudentDto update(StudentDto studentDto) throws EntityNotPresent, ExistEntityException;
	
	Page<StudentDto> findByPage(Pageable pageable);
	
	
}
