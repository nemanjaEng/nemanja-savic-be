package com.engineering.nemanja.savic.be.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.engineering.nemanja.savic.be.dao.CityDao;
import com.engineering.nemanja.savic.be.dao.StudentDao;
import com.engineering.nemanja.savic.be.dto.StudentDto;
import com.engineering.nemanja.savic.be.entity.CityEntity;
import com.engineering.nemanja.savic.be.entity.StudentEntity;
import com.engineering.nemanja.savic.be.mapper.StudentMapper;
import com.engineering.nemanja.savic.be.service.StudentService;
import com.engineering.nemanja.savic.exception.EntityNotPresent;
import com.engineering.nemanja.savic.exception.ExistEntityException;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {

	private final StudentDao studentDao;
	private final CityDao cityDao;


	private final StudentMapper studentMapper = Mappers.getMapper(StudentMapper.class);
	

	@Autowired
	public StudentServiceImpl(StudentDao studentDao, CityDao cityDao ) {

		this.studentDao = studentDao;
		this.cityDao = cityDao;
		
	}

	@Override
	public List<StudentDto> findAll() {
		List<StudentEntity> entites = studentDao.findAll();
		return entites.stream().map(entity -> {
			return studentMapper.toStudentDto(entity);
		}).collect(Collectors.toList());
	}

	@Override
	public void deleteById(Long id) {

		studentDao.deleteById(id);
	}

	@Override
	public StudentDto save(StudentDto studentDto) throws ExistEntityException {
		
		Optional<StudentEntity> studentEntityOpt = studentDao.findByIndexNumber(studentDto.getIndexNumber());
		if (studentEntityOpt.isPresent()) {
			throw new ExistEntityException(studentEntityOpt.get(), "Student with that index number already exist!");
		}
		
		studentEntityOpt = studentDao.findByEmail(studentDto.getEmail());
		if (studentEntityOpt.isPresent()) {
			throw new ExistEntityException(studentEntityOpt.get(), "Student with that email already exist!");
		}
		
//		Optional<CityEntity> cityEntity = cityDao.findById(studentDto.getCityDto().getPostalCode());
//		System.out.println(cityEntity);
//		if (!cityEntity.isPresent()) {
//			//cityDao.save(cityMapper.toCity(studentDto.getCityDto()));
//			
//		}

		StudentEntity studentEntity = studentMapper.toStudent(studentDto);
		
		//studentEntity.setCity(cityEntity.get());
		studentEntity = studentDao.save(studentEntity);

		return studentMapper.toStudentDto(studentEntity);
	}


	@Override
	public Optional<StudentDto> findById(Long id) {

		Optional<StudentEntity> studentEntity = studentDao.findById(id);
		if (studentEntity.isPresent()) {
			return Optional.of(studentMapper.toStudentDto(studentEntity.get()));
		}
		return Optional.empty();
	}

	@Override
	public StudentDto update(StudentDto studentDto) throws EntityNotPresent,  ExistEntityException{
		Optional<StudentEntity> studentEntityOpt = studentDao.findById(studentDto.getId());
		if (studentEntityOpt == null)
			throw new EntityNotPresent(studentDto, "Student doesnt exist!");

		Optional<CityEntity> cityEntity = cityDao.findById(studentDto.getCityDto().getPostalCode());
		if (!cityEntity.isPresent()) {
			throw new EntityNotPresent(studentDto, "City doesn't exist!");
		}
		;
		 studentEntityOpt = studentDao.findByIndexNumber(studentDto.getIndexNumber());
		if (studentEntityOpt.isPresent()) {
			throw new ExistEntityException(studentEntityOpt.get(), "Student with that index number already exist!");
		}
		
		studentEntityOpt = studentDao.findByEmail(studentDto.getEmail());
		if (studentEntityOpt.isPresent()) {
			throw new ExistEntityException(studentEntityOpt.get(), "Student with that email already exist!");
		}
		

		StudentEntity studentEntity = studentEntityOpt.get();
		studentEntity.setStudentId(studentDto.getId());
		studentEntity.setAddress(studentDto.getAddress());
		studentEntity.setCurrentYearOfStudy(studentDto.getCurrentYearOfStudy());
		studentEntity.setEmail(studentDto.getEmail());
		studentEntity.setFirstName(studentDto.getFirstName());
		studentEntity.setLastName(studentDto.getLastName());
		studentEntity.setIndexNumber(studentDto.getIndexNumber());
		studentEntity.setIndexYear(studentDto.getIndexYear());
		studentEntity.setCity(cityEntity.get());
		studentEntity = studentDao.save(studentEntity);
		return studentMapper.toStudentDto(studentEntity);
	}

	@Override
	public Page<StudentDto> findByPage(Pageable pageable) {
		
		Page<StudentDto> entites = studentDao.findAll(pageable).map(studentMapper::toStudentDto);
		return entites;
	}


	
}
