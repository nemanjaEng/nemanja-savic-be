package com.engineering.nemanja.savic.be.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "exam")
public class ExamEntity implements Serializable {

	private static final long serialVersionUID = -8044927524789514169L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="exam_id", nullable = false)
	private Long examId;
	
	@Column(name="exam_date")
	private Date examDate;
	
	@Column(name="exam_status", columnDefinition = "boolean default false" )
	private Boolean  examStatus = false;
	
	@ManyToOne
	@JoinColumn(name = "subject_id")
	private SubjectEntity subject;
	
	public ExamEntity() {

	}

	public ExamEntity(Long examId, Date examDate, boolean examStatus, SubjectEntity subject) {
		super();
		this.examId = examId;
		this.examDate = examDate;
		this.examStatus = examStatus;
		this.subject = subject;
	}

	public Long getExamId() {
		return examId;
	}

	public void setExamId(Long examId) {
		this.examId = examId;
	}

	public Date getExamDate() {
		return examDate;
	}

	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}

	public boolean isExamStatus() {
		return examStatus;
	}

	public void setExamStatus(boolean examStatus) {
		this.examStatus = examStatus;
	}

	public SubjectEntity getSubject() {
		return subject;
	}

	public void setSubject(SubjectEntity subject) {
		this.subject = subject;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((examDate == null) ? 0 : examDate.hashCode());
		result = prime * result + ((examId == null) ? 0 : examId.hashCode());
		result = prime * result + (examStatus ? 1231 : 1237);
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExamEntity other = (ExamEntity) obj;
		if (examDate == null) {
			if (other.examDate != null)
				return false;
		} else if (!examDate.equals(other.examDate))
			return false;
		if (examId == null) {
			if (other.examId != null)
				return false;
		} else if (!examId.equals(other.examId))
			return false;
		if (examStatus != other.examStatus)
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ExamEntity [examId=" + examId + ", examDate=" + examDate + ", examStatus=" + examStatus + ", subject="
				+ subject + "]";
	}
	
	
}
