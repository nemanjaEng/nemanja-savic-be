package com.engineering.nemanja.savic.be.dto;

import java.io.Serializable;


import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class StudentDto  implements Serializable{

	private static final long serialVersionUID = -7257044356859406288L;

	
	private Long id;
	
	@Size(min = 4, max = 4, message="Index number must be 4 characters")
	@NotEmpty(message = "Index number is required...")
	private String indexNumber;
	
	@Size(min = 4, max = 4, message = "Index year must be 4 characters")
	@NotEmpty(message = "Index year is required...")
	private String indexYear;
	
	@Size(min = 3, max = 30, message = "First name minimum 3 characters")
	@NotEmpty(message = "First name is required...")
	private String firstName;
	
	@Size(min = 3, max = 30, message = "Last name minimum 3 characters")
	@NotEmpty(message = "Last name is required...")
	private String lastName;
	
	@Email(message = "Email should be valid")
	private String email;
	
	@Size(min = 3, max = 30, message = "Address minimum 3 characters")
	private String address;
	
	@NotNull
	@Min(value = 1
	private Integer currentYearOfStudy;
	
	private CityDto cityDto;
	
	public StudentDto() {
		
	}

	public StudentDto(@NotNull(message = "City id is required...") Long id,
			@Size(min = 4, max = 4, message = "Index number must be 4 characters") @NotEmpty(message = "Index number is required...") String indexNumber,
			@Size(min = 4, max = 4, message = "Index year must be 4 characters") @NotEmpty(message = "Index year is required...") String indexYear,
			@Size(min = 3, max = 30, message = "First name minimum 3 characters") @NotEmpty(message = "First name is required...") String firstName,
			@Size(min = 3, max = 30, message = "Last name minimum 3 characters") @NotEmpty(message = "Last name is required...") String lastName,
			@Email(message = "Email should be valid") String email,
			@Size(min = 3, max = 30, message = "Address minimum 3 characters") String address,
			@NotEmpty(message = "Current Year Of Study is required and must be grater than 0...") @Min(1) int currentYearOfStudy,
			CityDto cityDto) {
		super();
		this.id = id;
		this.indexNumber = indexNumber;
		this.indexYear = indexYear;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.currentYearOfStudy = currentYearOfStudy;
		this.cityDto = cityDto;
	}

	public Long getId() {
		return id;
	}

	public void setStudentId(Long id) {
		this.id = id;
	}

	public String getIndexNumber() {
		return indexNumber;
	}

	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}

	public String getIndexYear() {
		return indexYear;
	}

	public void setIndexYear(String indexYear) {
		this.indexYear = indexYear;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getCurrentYearOfStudy() {
		return currentYearOfStudy;
	}

	public void setCurrentYearOfStudy(int currentYearOfStudy) {
		this.currentYearOfStudy = currentYearOfStudy;
	}

	public CityDto getCityDto() {
		return cityDto;
	}

	public void setCityDto(CityDto cityDto) {
		this.cityDto = cityDto;
	}

	@Override
	public String toString() {
		return "StudentDto [studentId=" + id + ", indexNumber=" + indexNumber + ", indexYear=" + indexYear
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", address=" + address
				+ ", currentYearOfStudy=" + currentYearOfStudy + ", cityDto=" + cityDto + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((cityDto == null) ? 0 : cityDto.hashCode());
		result = prime * result + currentYearOfStudy;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((indexNumber == null) ? 0 : indexNumber.hashCode());
		result = prime * result + ((indexYear == null) ? 0 : indexYear.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentDto other = (StudentDto) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (cityDto == null) {
			if (other.cityDto != null)
				return false;
		} else if (!cityDto.equals(other.cityDto))
			return false;
		if (currentYearOfStudy != other.currentYearOfStudy)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (indexNumber == null) {
			if (other.indexNumber != null)
				return false;
		} else if (!indexNumber.equals(other.indexNumber))
			return false;
		if (indexYear == null) {
			if (other.indexYear != null)
				return false;
		} else if (!indexYear.equals(other.indexYear))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
}
