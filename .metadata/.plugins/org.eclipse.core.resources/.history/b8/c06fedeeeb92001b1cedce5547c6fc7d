package com.engineering.nemanja.savic.be.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import com.engineering.nemanja.savic.be.dto.StudentDto;
import com.engineering.nemanja.savic.be.service.CityService;
import com.engineering.nemanja.savic.be.service.StudentService;
import com.engineering.nemanja.savic.exception.ExistEntityException;

@RestController
@RequestMapping("student")
public class StudentRestController {

	
	private final StudentService studentService;
	
	@Autowired
	public StudentRestController( StudentService studentService,  CityService cityService) {
		
		this.studentService = studentService;
	}
	
	@GetMapping("all")
	public @ResponseBody ResponseEntity<List<StudentDto>> getAll() {
		return ResponseEntity.status(HttpStatus.OK).body(studentService.findAll());
	}
	
	@GetMapping("get/{id}")
	public @ResponseBody ResponseEntity<Object> get(@PathVariable Long id) {
		Optional<StudentDto> studentDto = studentService.findById(id);
		if (studentDto.isPresent()) {
			return ResponseEntity.status(HttpStatus.OK).body(studentDto.get());
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Invalid student id!");
	}
	
	@DeleteMapping("delete/{id}")
	public @ResponseBody ResponseEntity<String> delete(@PathVariable Long id) {
		studentService.deleteById(id);
		return ResponseEntity.status(HttpStatus.OK).body("Deleted!");
	}
	
	@PostMapping("save")
	public @ResponseBody ResponseEntity<?> save(@Valid @RequestBody StudentDto studentDto) {
		try { 
		
			return ResponseEntity.status(HttpStatus.OK).body(studentService.save(studentDto));
		} catch (ExistEntityException ex) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
		}
	}
	@PostMapping(path = "/update")
	public @ResponseBody ResponseEntity<?> update(@Valid @RequestBody StudentDto studentDto){
		try {
			
			studentDto = studentService.update(studentDto);
			return ResponseEntity.status(HttpStatus.OK).body(studentDto);
		}catch(ExistEntityException ex) {
			ex.printStackTrace();
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
		}
	}
}
