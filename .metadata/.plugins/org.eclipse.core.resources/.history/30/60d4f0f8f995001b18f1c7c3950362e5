package com.engineering.nemanja.savic.be.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.engineering.nemanja.savic.be.dao.CityDao;
import com.engineering.nemanja.savic.be.dao.ProfessorDao;

import com.engineering.nemanja.savic.be.dao.TitleDao;
import com.engineering.nemanja.savic.be.dto.ProfessorDto;
import com.engineering.nemanja.savic.be.entity.CityEntity;
import com.engineering.nemanja.savic.be.entity.ProfessorEntity;
import com.engineering.nemanja.savic.be.entity.StudentEntity;
import com.engineering.nemanja.savic.be.entity.TitleEntity;
import com.engineering.nemanja.savic.be.mapper.ProfessorMapper;

import com.engineering.nemanja.savic.be.service.ProfessorService;
import com.engineering.nemanja.savic.exception.EntityNotPresent;
import com.engineering.nemanja.savic.exception.ExistEntityException;

@Service
@Transactional
public class ProfessorServiceImpl implements ProfessorService {

	private final ProfessorDao professorDao;
	private final CityDao cityDao;
	private final TitleDao titleDao;

	private final ProfessorMapper professorMapper = Mappers.getMapper(ProfessorMapper.class);

	@Autowired
	public ProfessorServiceImpl(ProfessorDao professorDao, CityDao cityDao, TitleDao titleDao) {
		super();
		this.professorDao = professorDao;
		this.cityDao = cityDao;
		this.titleDao = titleDao;
	}

	@Override
	public List<ProfessorDto> findAll() {

		List<ProfessorEntity> entites = professorDao.findAll();
		return entites.stream().map(entity -> {
			return professorMapper.toProfessorDto(entity);
		}).collect(Collectors.toList());
	}

	@Override
	public void deleteById(Long id) {

		professorDao.deleteById(id);
	}

	@Override
	public ProfessorDto save(ProfessorDto professorDto) throws ExistEntityException {

		ProfessorEntity professorEntity = professorMapper.toProfessor(professorDto);

		Optional<TitleEntity> titleEntity = titleDao.findById(professorDto.getTitle().getTitleNumber());
		if (!titleEntity.isPresent()) {
			throw new ExistEntityException(professorDto, "Title is mandatory!");
		}

		// studentEntity.setCity(cityEntity.get());
		professorEntity = professorDao.save(professorEntity);

		return professorMapper.toProfessorDto(professorEntity);
	}

	@Override
	public Optional<ProfessorDto> findById(Long id) {
		Optional<ProfessorEntity> professorEntity = professorDao.findById(id);
		if (professorEntity.isPresent()) {
			return Optional.of(professorMapper.toProfessorDto(professorEntity.get()));
		}
		return Optional.empty();
	}

	@Override
	public ProfessorDto update(ProfessorDto professorDto) throws EntityNotPresent {
		Optional<ProfessorEntity> professorEntityOpt = professorDao.findById(professorDto.getId());
		if (professorEntityOpt == null)
			throw new EntityNotPresent(professorDto, "Professor doesnt exist!");

		Optional<TitleEntity> titleEntity = titleDao.findById(professorDto.getTitle().getTitleNumber());
		if (!titleEntity.isPresent()) {
			throw new EntityNotPresent(professorDto, "Title is mandatory");
		}
		;
		Optional<CityEntity> cityEntity = cityDao.findById(professorDto.getCity().getPostalCode());
		if (!cityEntity.isPresent()) {
			throw new EntityNotPresent(professorDto, "City doesnt exist");
		}
		;

		ProfessorEntity professorEntity = professorEntityOpt.get();
		professorEntity.setId(professorDto.getId());
		professorEntity.setFirstName(professorDto.getFirstName());
		professorEntity.setLastName(professorDto.getLastName());
		professorEntity.setEmail(professorDto.getEmail());
		professorEntity.setAddress(professorDto.getAddress());
		professorEntity.setPhone(professorDto.getPhone());
		professorEntity.setReelectionDate(professorDto.getReelectionDate());
		professorEntity.setCity(cityEntity.get());
		professorEntity.setTitle(titleEntity.get());
		professorEntity = professorDao.save(professorEntity);
		
		return professorMapper.toProfessorDto(professorEntity);

	}

	@Override
	public Page<ProfessorDto> findByPage(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

}
