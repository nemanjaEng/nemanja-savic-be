package com.engineering.nemanja.savic.be.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.engineering.nemanja.savic.be.dto.ProfessorDto;

@Entity
@Table(name = "subject")
public class SubjectEntity  implements Serializable {

	private static final long serialVersionUID = 8503673925341679639L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id", nullable = false)
	private Long id;
	
	@Column(name="name", nullable = false, unique = false, length =30)
	private String name;
	
	@Column(name="description", nullable = true, unique = false, length =200)
	private String description;
	
	@Column(name="no_of_esp", nullable = false, unique = false)
	@Min(value = 1)
	@Max(value = 9)
	private Integer noOfeSP;
	
	@Column(name="year_of_study", nullable = true, unique = false, length = 1)
	@Min(value = 1)
	@Max(value = 5)
	private Integer yearOfStudy;
	
	
	@Column(name = "semester", nullable = true, unique = false, length = 10)
	@Enumerated(EnumType.STRING)
	private Semester semester;
	
	@ManyToOne
	@JoinColumn(name = "id", referencedColumnName = "", table = "subject" )
	private ProfessorEntity professor;
	
	public SubjectEntity() {
	
	}

	public SubjectEntity(Long id, String name, String description, @Min(1) @Max(9) Integer noOfeSP,
			@Min(1) @Max(5) Integer yearOfStudy, Semester semester, ProfessorEntity professor) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.noOfeSP = noOfeSP;
		this.yearOfStudy = yearOfStudy;
		this.semester = semester;
		this.professor = professor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((noOfeSP == null) ? 0 : noOfeSP.hashCode());
		result = prime * result + ((professor == null) ? 0 : professor.hashCode());
		result = prime * result + ((semester == null) ? 0 : semester.hashCode());
		result = prime * result + ((yearOfStudy == null) ? 0 : yearOfStudy.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubjectEntity other = (SubjectEntity) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (noOfeSP == null) {
			if (other.noOfeSP != null)
				return false;
		} else if (!noOfeSP.equals(other.noOfeSP))
			return false;
		if (professor == null) {
			if (other.professor != null)
				return false;
		} else if (!professor.equals(other.professor))
			return false;
		if (semester != other.semester)
			return false;
		if (yearOfStudy == null) {
			if (other.yearOfStudy != null)
				return false;
		} else if (!yearOfStudy.equals(other.yearOfStudy))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SubjectEntity [id=" + id + ", name=" + name + ", description=" + description + ", noOfeSP=" + noOfeSP
				+ ", yearOfStudy=" + yearOfStudy + ", semester=" + semester + ", professor=" + professor + "]";
	}

	
	
	
}
