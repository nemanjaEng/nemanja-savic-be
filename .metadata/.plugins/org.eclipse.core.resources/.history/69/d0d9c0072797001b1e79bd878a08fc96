package com.engineering.nemanja.savic.be.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import com.engineering.nemanja.savic.be.dto.StudentDto;
import com.engineering.nemanja.savic.be.service.StudentService;
import com.engineering.nemanja.savic.exception.EntityNotPresent;
import com.engineering.nemanja.savic.exception.ExistEntityException;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/student")
public class StudentRestController {

	
	private final StudentService studentService;
	
	@Autowired
	public StudentRestController( StudentService studentService) {
		
		this.studentService = studentService;
	}
	
	@GetMapping()
	public @ResponseBody ResponseEntity<List<StudentDto>> getAll() {
		return ResponseEntity.status(HttpStatus.OK).body(studentService.findAll());
	}
	
	@GetMapping("/page")
	public @ResponseBody ResponseEntity<Page<StudentDto>> getByPage(Pageable pageable) {
		return ResponseEntity.status(HttpStatus.OK).body(studentService.findByPage(pageable));
	}
	
	@GetMapping("/{id}")
	public @ResponseBody ResponseEntity<Object> get(@PathVariable Long id) {
		Optional<StudentDto> studentDto = studentService.findById(id);
		if (studentDto.isPresent()) {
			return ResponseEntity.status(HttpStatus.OK).body(studentDto.get());
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Invalid student id!");
	}
	
	@DeleteMapping("/{id}")
	public @ResponseBody ResponseEntity<String> delete(@PathVariable Long id) {
		studentService.deleteById(id);
		return ResponseEntity.status(HttpStatus.OK).body("Deleted!");
	}
	
	@PostMapping()
	public @ResponseBody ResponseEntity<Object> save(@Valid @RequestBody StudentDto studentDto) {
		try { 
		System.out.println(studentDto);
			return ResponseEntity.status(HttpStatus.OK).body(studentService.save(studentDto));
		} catch (ExistEntityException ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}
	@PutMapping(path = "/{id}")
	public @ResponseBody ResponseEntity<?> update(@PathVariable Long id,@Valid @RequestBody StudentDto studentDto) throws EntityNotPresent, ExistEntityException{
		try {
			
			studentDto = studentService.update(studentDto);
			return ResponseEntity.status(HttpStatus.OK).body(studentDto);
		}catch(EntityNotPresent ex) {
			ex.printStackTrace();
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
		}
	}
}
